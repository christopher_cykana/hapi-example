const ProductHandler = function (request, reply) {
  const start = Date.now()
  const resource = '/V1/products/' + request.params.sku

  const filterAttribute = function (data, attributeName) {
    const filtered = data.custom_attributes.filter(attr => {
      return attr.attribute_code === attributeName
    })

    if (filtered.length > 0) {
      return filtered[0].value
    }
  }

  m2cache.get(resource, (err, value, cached, report) => {
    console.log('%s in %dms %s %s',
      resource,
      Date.now() - start,
      cached ? '(CACHED)' : '',
      cached && cached.isStale ? '(STALE)' : ''
    )

    const heroImage = value.media_gallery_entries.reduce((memo, imageData) => {
      if (imageData.types.indexOf('thumbnail') !== -1) {
        memo = 'https://cdn.2ndwindexercise.com/media/product' + imageData.file
      }
      return memo
    }, '')

    const description = filterAttribute(value, 'description')
    const specs = filterAttribute(value, 'specs')

    console.log('hero image path:', heroImage)

    reply.view('product', {
      message: 'Hi there!',
      name: value.name,
      sku: value.sku,
      price: value.price,
      description: description,
      heroImage: heroImage,
      specs: specs,
      productData: JSON.stringify(value, null, 2)
    })
  })
}

module.exports = ProductHandler
