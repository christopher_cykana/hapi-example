const React = require('react');
const Layout = require('./layout.jsx');

const styles = {
  mt1: {
    'marginTop': '1rem'
  }
}

class ProductArticle extends React.Component {

  renderImage (imageUrl) {
    return (
      <img src={imageUrl} />
    )
  }

  maybeRenderSpecs (specs) {

    if (typeof specs === 'undefined') {
      return
    }

    return (
      <div className="row" style={styles.mt1}>
        <div className="small-12 columns">
          <h3>Specs</h3>
          <div dangerouslySetInnerHTML={{__html: specs}}></div>
        </div>
      </div>
    )
  }

  render () {

    return (
      <Layout>
        <article className="row" style={styles.mt1}>
          <header className="small-6 columns">
            <h3>{this.props.name} <small>{this.props.sku}</small></h3>
            <p dangerouslySetInnerHTML={{__html: this.props.description}}></p>
          </header>
          <div className="small-6 columns">
            {this.renderImage(this.props.heroImage)}
          </div>
        </article>
        {this.maybeRenderSpecs(this.props.specs)}
        <div className="row" style={styles.mt1}>
          <div class="small-12 columns">
            <div className="callout secondary">
              <pre>
                {this.props.productData}
              </pre>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

module.exports = ProductArticle;
