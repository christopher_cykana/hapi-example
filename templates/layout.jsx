var React = require('react');
var Head = require('./includes/head.jsx');
var Foot = require('./includes/foot.jsx');

const styles = {
  logo: {
    maxWidth: '150px',
    marginTop: '1rem'
  }
}

var Component = React.createClass({
  render: function() {
    return (
      <html>
        <Head />
        <body>
          <div className="row">
            <div className="small-12 columns">
              <img src="https://med.2ndwindexercise.com/logo/default/2nd_Wind_Logo_1.jpg" alt="2nd Wind Exercise" style={styles.logo} />
            </div>
          </div>
          {this.props.children}
          <Foot />
        </body>
      </html>
    );
  }
});

module.exports = Component;
