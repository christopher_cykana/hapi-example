var React = require('react');
var Layout = require('./layout.jsx');



var CmsPage = React.createClass({
  render: function() {
    return (
      <Layout>
        <div className="row">
          <div className="small-12 columns">
            <h1>{this.props.title}</h1>
            <div dangerouslySetInnerHTML={{__html: this.props.cmsContent}}></div>
          </div>
        </div>
      </Layout>
    );
  }
});

module.exports = CmsPage;
