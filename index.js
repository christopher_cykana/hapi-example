const Hapi = require('hapi')
const Inert = require('inert')
const Path = require('path')
const Magento2 = require('node-magento2')
const Catbox = require('catbox')
const CatboxMemory = require('catbox-redis')
const Vision = require('vision')

require('babel-core/register')({
    plugins: ['transform-react-jsx']
});

const server = new Hapi.Server({
  connections: {
  routes: {
    files: {
        relativeTo: __dirname
      }
    }
  },
  cache: {
    engine: require('catbox-redis'),
    name: 'redis-cache',
    database: 7
  }
})

const m2options = {
  authentication: {
    login: {
      username: 'sw_magento_admin',
      password: 'ck82Q6UhPPX32982DZve'
    }
  }
}

const m2 = new Magento2('https://www-stage.2ndwindexercise.com/', m2options)
m2.init()


const m2call = function (resource, cb) {
  m2.get(resource).then(payload => {
    cb(null, payload)
  })
}

const m2cache = server.cache({
  generateFunc: m2call,
  expiresIn: 60000,
  staleIn: 10000,
  staleTimeout: 100,
  cache: 'redis-cache',
  segment: 'm2',
  generateTimeout: 10000
})

const ProductHandler = function (request, reply) {
  const start = Date.now()
  const resource = '/V1/products/' + request.params.sku

  const filterAttribute = function (data, attributeName) {
    const filtered = data.custom_attributes.filter(attr => {
      return attr.attribute_code === attributeName
    })

    if (filtered.length > 0) {
      return filtered[0].value
    }
  }

  m2cache.get(resource, (err, value, cached, report) => {
    console.log('%s in %dms %s %s',
      resource,
      Date.now() - start,
      cached ? '(CACHED)' : '',
      cached && cached.isStale ? '(STALE)' : ''
    )

    const heroImage = value.media_gallery_entries.reduce((memo, imageData) => {
      if (imageData.types.indexOf('thumbnail') !== -1) {
        memo = 'https://cdn.2ndwindexercise.com/media/product' + imageData.file
      }
      return memo
    }, '')

    const description = filterAttribute(value, 'description')
    const specs = filterAttribute(value, 'specs')

    console.log('hero image path:', heroImage)

    reply.view('product', {
      message: 'Hi there!',
      name: value.name,
      sku: value.sku,
      price: value.price,
      description: description,
      heroImage: heroImage,
      specs: specs,
      productData: JSON.stringify(value, null, 2)
    })
  })
}

server.connection({
    port: Number(process.argv[2] || 8080)
})

// The 'onRequest' event fires before Hapi starts
// going through its routing table. This might be
// a good place to do whatever magic needs to happen
// to translate a url_key to a catalog entity
const onRequestExt = function (request, reply) {
  request.app.catalogRoute = request.path
  console.log(request.app.catalogRoute)
  reply.continue()
}

server.ext('onRequest', onRequestExt)

server.register([Inert, Vision], (err) => {

    if (err) {
        throw err
    }

    server.views({
      engines: { jsx: require('hapi-react-views') },
      path: __dirname + '/templates',
      compileOptions: {
        pretty: true
      }
    })
})

server.route([{
    method: 'GET',
    path: '/{path*}',
    handler (request, reply) {
      reply.view('CmsPage', {
        title: 'examples/views/jsx/index.js | Hapi ' + request.server.version,
        message: 'Index - Hello World!'
      })
    }
}, {
    method: 'GET',
    path: '/json',
    handler (request, reply) {
        reply({ hello: 'World' })
    }
}, {
    method: 'GET',
    path: '/product/{sku*}',
    handler: ProductHandler
}, {
    method: 'GET',
    path: '/page/{pageId}',
    handler (request, reply) {
        const start = Date.now()
        const resource = '/V1/cmsPage/' + request.params.pageId

        m2cache.get(resource, (err, value, cached, report) => {
            console.log('%s in %dms %s %s',
                resource,
                Date.now() - start,
                cached ? '(CACHED)' : '',
                cached && cached.isStale ? '(STALE)' : ''
            )

            reply.view('CmsPage', {
              cmsContent: value.content
            })
        })
    }
}, {
    method: 'GET',
    path: '/block/{blockId}',
    handler (request, reply) {
        const start = Date.now()
        const resource = '/V1/cmsBlock/' + request.params.blockId

        m2cache.get(resource, (err, value, cached, report) => {
            console.log('%s in %dms %s %s',
                resource,
                Date.now() - start,
                cached ? '(CACHED)' : '',
                cached && cached.isStale ? '(STALE)' : ''
            )
            reply(value.content)
        })
    }
}])

server.start((err) => {
    if (err) throw err
    console.log('Server running at:', server.info.uri)
})
